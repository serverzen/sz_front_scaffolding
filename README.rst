====================
sz_front_scaffolding
====================

Introduction
============

This is a scaffolding package that provides:

  - `jQuery <http://jquery.com/>`_
  - `Twitter Bootstrap <http://twitter.github.io/bootstrap/>`_
  - `Knockout <http://knockoutjs.com/>`_
  - `RequireJS <http://requirejs.org/>`_

Usage
=====

The static assets are available to anything that wants to pull them in via the ``static``
directory within the ``sz_front_scaffolding`` package.

Jinja2 Macros
-------------

  1. Add the ``jinja2/templates`` directory of the ``sz_front_scaffolding`` package
     to the jinja path.

  2. Include the ``macros.jinja2`` template and use the given *RequireJS* macros
     as follows::

       {% from "sz_front_scaffolding.jinja2:templates/macros.jinja2" import js_require %}
       {{ js_require(request) }}

Credits
=======

Developed and maintained by *Rocky Burt (rocky AT serverzen DOT com)*.
