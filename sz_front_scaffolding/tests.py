import unittest

HAS_PYRAMID = False
try:
    import pyramid
    HAS_PYRAMID = True
except ImportError: pass


class Mock(object):
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)


class MockConfig(object):
    def __init__(self):
        self.static_views = []

    def add_static_view(self, *args, **kwargs):
        self.static_views.append(Mock(args=args, kwargs=kwargs))


if HAS_PYRAMID:
    class PyramidTests(unittest.TestCase):
        def test_it(self):
            from sz_front_scaffolding.jinja2.pyramid import includeme
            config = MockConfig()
            includeme(config)
            self.assertTrue(len(config.static_views), 1)
            self.assertTrue('path' in config.static_views[0].kwargs)
            self.assertTrue('path' in config.static_views[0].kwargs)
